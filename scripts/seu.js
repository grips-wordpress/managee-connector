let $ = jQuery;
$(window).load(function () {

 $('.managee.managee-seu').each(function() {

     let currentStart = $(this).attr("data-start");
     let currentEnd = $(this).attr("data-end");

     let dateFormat = "dd.mm.yy";

     let downloadExcel = function () {
         window.location.href = endpoint_get_seu_data.url+"?action="+endpoint_get_xlsx_data.action+"&nonce="+endpoint_get_xlsx_data.nonce + "&start_date=" + currentStart + "&end_date=" + currentEnd;
     }

     let getDate = function( element ) {
         let date;
         try {
             date = $.datepicker.parseDate( dateFormat, element.value );
         } catch( error ) {
             date = null;
         }

         return date;
     }

     let datePickerStart = $('<input type="text" class="datepicker" name="datepicker" style="width: auto; display: inline-block;">');
     let datePickerEnd = $('<input type="text" class="datepicker" name="datepicker" style="width: auto; display: inline-block;">');
     $(this).append($("<span>SEU von </span>"));
     $(this).append(datePickerStart);
     $(this).append($("<span> bis </span>"));
     $(this).append(datePickerEnd);

     $(this).append($('<button style="float:right;">Download Excel</button>').on('click', downloadExcel))

     let from = datePickerStart.datepicker({
         changeMonth: true,
         changeYear: true,
         dateFormat: dateFormat
     }).on( "change", function() {
         to.datepicker( "option", "minDate", getDate( this ) );
         currentStart = getDate(this).toISOString().slice(0,10);
         fetchData();
     });
     let to = datePickerEnd.datepicker({
         changeMonth: true,
         changeYear: true,
         dateFormat: dateFormat
     }).on( "change", function() {
         from.datepicker( "option", "maxDate", getDate( this ) );
         currentEnd = getDate(this).toISOString().slice(0,10);
         fetchData();
     });

     datePickerStart.datepicker('setDate', new Date(currentStart));
     datePickerEnd.datepicker('setDate', new Date(currentEnd));

     let dataView = $('<div class="data-view" style="margin-top: 15px;"></div>');

     $(this).append(dataView);

     console.log("allowModeSwitch: " + allowModeSwitch);
     console.log("showSettings: " + showSettings);
     let sort = "node";
     let fetchData = function () {
         let data = {
             'action': endpoint_get_seu_data.action,
             'nonce': endpoint_get_seu_data.nonce,
             'startDate': currentStart,
             'endDate': currentEnd,
             'allowModeSwitch': allowModeSwitch,
             'showSettings': showSettings,
             "sortBy": sort
         };

         dataView.html("lade daten...");

         $.post(endpoint_get_seu_data.url, data, function (response) {
             dataView.html(response);
             showSettings = dataView.find("#showSettings").val() === "true";
             console.log("showSettings:" + showSettings)
             dataView.find(".switch-mode-button").on('click', function() {
                 showSettings = !showSettings;
                 fetchData();
             });
             dataView.find('.sort-button').each(function() {
                 $(this).on('click', function() {
                     sort = $(this).attr("data-sort");
                     fetchData();
                 });
             });

             let saveButton = dataView.find(".save-button");
             saveButton.on("click", function() {
                 let formData = [];

                 dataView.find('.node-setting').each(function() {
                     formData.push({
                         node_id: $(this).attr("data-node-id"),
                         seu_visible: $(this).find(".node_visible_checkbox").is(":checked")?1:0,
                         alias: $(this).find(".alias-input").val(),
                         description: $(this).find(".description-input").val(),
                     })
                 });

                 let data = {
                     'action': endpoint_save_additional_data.action,
                     'nonce': endpoint_save_additional_data.nonce,
                     'data': formData
                 };

                 $.post(endpoint_save_additional_data.url, data, function (response) {
                     if(response === "saved") {
                         alert("Einstellungen wurden gespeichert!");
                     } else {
                         alert(response);
                     }
                 });
             });

         });
     }

     fetchData();
 });
});