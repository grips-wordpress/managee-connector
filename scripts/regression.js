let $ = jQuery;
$(window).load(function () {

    let months = [
        "Januar",
        "Februar",
        "März",
        "April",
        "Mai",
        "Juni",
        "Juli",
        "August",
        "September",
        "Oktober",
        "November",
        "Dezember"
    ];

    let calculateTrendlineSlope = function (graph) {
        let n = graph.length;
        let a = 0;
        let b = 0;
        let bx = 0;
        let by = 0;
        let c = 0;
        let d = 0;

        let e = 0;
        let g = 0;
        let h = 0;
        let f = 0;

        graph.forEach(function (point) {
            a += point.x * point.y;
            bx += point.x;
            by += point.y;
            c += point.x * point.x;
            d += point.x;
            e += point.y;
            g += point.y * point.y;
            h += point.x * point.x;
            f += point.x * point.y;
        });
        a *= n;
        b = bx * by;
        c *= n;
        d = d * d;

        let slope = (a - b) / (c - d);

        let intercept = (e - (slope * bx)) / n;

        let r =
            (f - (1 / n) * bx * e) /
            Math.sqrt((h - (1 / n) * bx * bx) * (g - (1 / n) * by * by));

        return {
            slope: slope,
            intercept: intercept,
            r2: r * r
        };
    }

    let compare = function (a, b) {
        if (a.x < b.x) {
            return -1;
        }
        if (a.x > b.x) {
            return 1;
        }
        return 0;
    }

    chart_data.forEach(function (chartData) {

        let currentNodeId = 0;
        let currentNodeYear = 2021;

        let chartDiv = $('#regression_' + chartData.chartId);
        let measurementNodesById = {};
        let measurementNodesList = measurement_nodes;
        for (let i = 0; i < measurementNodesList.length; i++) {
            let node = measurementNodesList[i];
            measurementNodesById[node['id']] = node;
        }

        /**
         * Convert a string to HTML entities
         */
        String.prototype.toHtmlEntities = function () {
            return this.replace(/./gm, function (s) {
                // return "&#" + s.charCodeAt(0) + ";";
                return (s.match(/[a-z0-9\s]+/i)) ? s : "&#" + s.charCodeAt(0) + ";";
            });
        };

        /**
         * Create string from HTML entities
         */
        String.fromHtmlEntities = function (string) {
            return (string + "").replace(/&#\d+;/gm, function (s) {
                return String.fromCharCode(s.match(/\d+/gm)[0]);
            })
        };

        let currentData = [];
        let activeCharts = [];

        let rebuildDataTableHtml = function (data) {
            currentData = data;
            delete currentData.raw;
            let table = chartDiv.find(".regression-data-table");
            table.html("<thead></thead><tbody></tbody>");
            console.log(currentData);
            let headerRow = $('<tr></tr>');
            headerRow.append($('<th>Monat</th>'));
            headerRow.append($('<th>Stromverbrauch (kWh)</th>'));

            //Header
            data.columns.forEach(function (column) {
                let headerColumn = $('<th style="white-space: nowrap;"><input style="display: inline-block; width: auto;" type="text" value="' + column + '"> <a class="remove-button" style="color: #a02a2a;" href="#"><span class="dashicons dashicons-trash"></span></a></th>');
                headerColumn.find("input").on("change", function () {
                    renameColumn(column, $(this).val().toHtmlEntities());
                });
                headerColumn.find(".remove-button").on('click', function () {
                    if (confirm("wollen Sie wirklich den Datensatz '" + column + "' für dieses Jahr Löschen?")) {
                        removeColumn(column);
                    }
                });
                headerRow.append(headerColumn);
            });

            let addColumn = $('<th><a href="#" style="color: green;" class="add-button"><span class="dashicons dashicons-plus"></span></a></th>')
            addColumn.on("click", function () {
                addNewColumn();
            });
            headerRow.append(addColumn);

            table.find("thead").append(headerRow);


            //Body
            for (let i = 0; i < 12; i++) {
                let month = months[i];

                let monthRow = $('<tr></tr>');

                monthRow.append($('<td>' + month + '</td>'))
                monthRow.append($('<td>' + currentData.data[i].energy + '</td>'))

                data.columns.forEach(function (column) {
                    let value = 0;
                    if(data.data[i].columns !== undefined && data.data[i].columns[column] !== undefined) {
                        value = data.data[i].columns[column];
                    }
                    if(currentData.data[i].columns === undefined) {
                        currentData.data[i].columns = [];
                    }
                    currentData.data[i].columns[column] = value;
                    let columnInput = $('<td><input type="text" value="' + value + '"></td>');
                    monthRow.append(columnInput);
                    columnInput.find("input").on("change", function () {
                        currentData.data[""+i].columns[""+column] = $(this).val();
                        updateData();
                    });
                });

                //Empty column for add button
                monthRow.append($('<td></td>'))

                table.find("tbody").append(monthRow);
            }
        }

        let removeColumn = function (columnName) {
            currentData.columns.splice(currentData.columns.indexOf(columnName), 1);
            for (let i = 0; i < 12; i++) {
                delete (currentData.data[i].columns[columnName]);
            }
            rebuildDataTableHtml(currentData);
            updateData();
        }

        let renameColumn = function (oldName, newName) {
            while (currentData.columns.indexOf(newName) !== -1) {
                newName += "_1";
            }
            currentData.columns.splice(currentData.columns.indexOf(oldName), 1, newName);
            for (let i = 0; i < 12; i++) {
                currentData.data[i].columns[newName] = currentData.data[i].columns[oldName];
                delete (currentData.data[i].columns[oldName]);
            }
            rebuildDataTableHtml(currentData);
            updateData();
        }

        let addNewColumn = function () {
            let i = 1;
            let newName = "Neuer Datensatz";
            while (currentData.columns.indexOf(newName) !== -1) {
                newName = "Neuer Datensatz " + i;
                i++;
            }
            currentData.columns.push(newName);
            for (let i = 0; i < 12; i++) {
                if(currentData.data[i].columns === undefined) {
                    currentData.data[i].columns = [];
                }
                currentData.data[i].columns[newName] = 0;
            }
            rebuildDataTableHtml(currentData);
            updateData();
        }


        let getMeasurementNodeSelect = function (nodeList) {
            let nodeSelect = $('<select style="max-width: 400px; display: inline-block; margin-right: 10px; margin-bottom: 10px;"></select>');
            // noinspection JSUnresolvedVariable
            for (let i = 0; i < nodeList.length; i++) {
                let node = nodeList[i];
                let id = node['id'];
                let selected = "";
                if (i === 0) {
                    currentNodeId = id;
                    selected = " selected";
                }

                let prefix = '';
                for (let j = 0; j < node['depth']; j++) {
                    prefix += '&nbsp;';
                }
                let name = prefix + node['name'];
                nodeSelect.append($('<option value="' + id + '"' + selected + '>' + name + '</option>'));
            }
            nodeSelect.on('change', function () {
                currentNodeId = $(this).val();
                fetchData();
            });
            return nodeSelect;
        }

        let getYearSelect = function (maxYear, currentYear) {
            let yearSelect = $('<select style="max-width: 200px; display: inline-block; margin-bottom: 10px;"></select>');
            // noinspection JSUnresolvedVariable
            for (let i = 2010; i <= maxYear; i++) {
                let selected = "";
                if (i === parseInt(currentYear)) {
                    currentNodeYear = i;
                    selected = " selected";
                }
                yearSelect.append($('<option value="' + i + '"' + selected + '>' + i + '</option>'));
            }
            yearSelect.on('change', function () {
                currentNodeYear = $(this).val();
                fetchData();
            });
            return yearSelect;
        }

        let fetchData = function () {
            let data = {
                'action': endpoint_get_node_regression_data.action,
                'nonce': endpoint_get_node_regression_data.nonce,
                'node_id': currentNodeId,
                'year': currentNodeYear
            };
            chartDiv.find('.loading-container').show();
            $.post(endpoint_get_node_regression_data.url, data, function (response) {

                rebuildDataTableHtml(response);
                /*for(let i = 0; i < 12; i++) {
                    chartDiv.find(".month-energy[data-id="+i+"]").html(response[i].energy);
                    chartDiv.find(".month-value[data-id="+i+"]").val(response[i].count);
                }*/
                chartDiv.find('.loading-container').hide();
                updateData();

            });
        }

        let updateData = function () {
            let i = 0;

            activeCharts.forEach(function(chart) {
                chart.destroy();
            });
            activeCharts = [];

            let chartContainer = chartDiv.find('.chart-container');
            chartContainer.html("");


            currentData.columns.forEach(function (column) {
                let datasets = [];
                let data = [];
                let minX = 10000000000;
                let maxX = 0;
                let maxY = 0;
                let values = [];

                let newChart = $('<div style="width: 100%; max-width: 800px; display: inline-block; overflow: visible;"><h4>'+column+'</h4><h5>r&sup2; = <span class="r2-result"></span></h5><div style="width: 100%;"><canvas id="scatterChart"></canvas></div></div>');
                chartContainer.append(newChart);
                let labels = [];
                for (let i = 0; i < 12; i++) {
                    let x = parseFloat(currentData.data[i].energy);
                    let y = parseFloat(currentData.data[i].columns[column]);
                    if (x < minX) {
                        minX = x;
                    }
                    if (x > maxX) {
                        maxX = x;
                    }
                    if (y > maxY) {
                        maxY = y;
                    }
                    values.push({
                        x: x,
                        y: y,
                        label: months[i]
                    });
                }


                values.sort(compare);
                let trend = calculateTrendlineSlope(values);
                values.forEach(function (element) {
                    labels.push(element.label);
                    data.push({
                        x: element.x,
                        y: element.y
                    });
                });
                newChart.find('.r2-result').html(trend.r2);


                datasets.push({
                    label: String.fromHtmlEntities(column),
                    borderColor: 'rgb(255, 99, 132, 1)',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderWidth: 0,
                    fill: false,
                    showLine: false,
                    data: data
                }, {
                    label: 'Trend ' + String.fromHtmlEntities(column),
                    data: [
                        {
                            x: 0,
                            y: trend.intercept
                        }, {
                            x: maxX * 1.2,
                            y: (maxX * 1.2) * trend.slope + trend.intercept
                        }
                    ],
                    pointRadius: 0,
                    borderColor: 'rgb(141,183,105)',
                    backgroundColor: 'rgba(141,183,105)',
                    fill: false
                });

                let ctx = newChart.find('#scatterChart')[0].getContext('2d');
                setTimeout(function() {
                    activeCharts.push(new Chart(ctx, {
                        type: 'scatter',
                        data: {
                            datasets: datasets,
                            labels: labels
                        },
                        options: {
                            tooltips: {
                                callbacks: {
                                    label: function (tooltipItem, data) {
                                        console.log(tooltipItem);
                                        let label = labels[tooltipItem.index];
                                        return label + ": " + tooltipItem.xLabel + ' / ' + tooltipItem.yLabel;
                                    }
                                }
                            },
                            scales: {
                                xAxes: [{
                                    id: 'energy',
                                    type: 'linear',
                                    position: 'bottom',
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Stromverbrauch (kWh)'
                                    }
                                }],
                                yAxes: [{
                                    type: 'linear',
                                    position: 'left',
                                    scaleLabel: {
                                        display: true,
                                        labelString: String.fromHtmlEntities(column)
                                    }
                                }]
                            }
                        }
                    }));
                }, 100);

                i++;
            });
        }


        chartDiv.find('.save-button').on('click', function() {
            saveData(currentData);
        })

        let saveData = function (data) {

            let dataNew = {
                columns: data.columns,
                months: []
            };

            for(let i = 0; i < 12; i++) {
                dataNew.months[i] = [];
                let j = 0;
                dataNew.columns.forEach(function(column) {
                    dataNew.months[i][j] = data.data[i].columns[column];
                    j++;
                });
            }

            let postData = {
                'action': endpoint_put_node_regression_data.action,
                'nonce': endpoint_put_node_regression_data.nonce,
                'node_id': currentNodeId,
                'year': currentNodeYear,
                'data': dataNew
            };

            console.log(dataNew);

            $.post(endpoint_put_node_regression_data.url, postData, function (response) {
                alert("Daten wurden gespeichert!");
            });
        }

        if (chartData.nodeId !== null) {
            currentNodeId = chartData.nodeId;
        } else {
            chartDiv.find(".select-div").append(getMeasurementNodeSelect(measurement_nodes));
        }
        chartDiv.find(".select-div").append(getYearSelect(current_year, chartData.year));

        fetchData();

    });

});