ManageE Connector
=================
This Plugin creates a central interface for all ManageE Plugins. It contains settings for API endpoint connections as well as Utility functions for sub-plugins.

Updates
=======
To create a new Update for this Plugin, you simply change the version number of the `managee-connector.php` files header to the new Version number. Then you can add changelogs to the `readme.txt` file. Finally you push the new version to the gitlab repository and add a new tag with the version number as its name. Now older versions can update to the new Version using the Wordpress Plugin Update view.

To update the Database for new Version please add new lines to the `/updates/database.sql` file. **Important:** Please do not remove or change old lines in the file. Only add new lines to the end of the file and make sure they are closed with a semicolon **`;`**.