=== ManageE Connector ===
Contributors: GriPS Automation GmbH
Donate link: https://managee.de
Tags: utility
Requires at least: 5.0
Tested up to: 5.5.3
Stable tag: 4.3
Requires PHP: 7.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

ManageE Connector Plugin that supports a variety of Wordpress Plugins.

== Description ==

ManageE Connector Plugin that supports a variety of Wordpress Plugins.

== Frequently Asked Questions ==
= Welche Plugins Unterstützt der ManageE Connetor? =

Die Aktuelle Liste der ManageE Plugins findes du unter https://managee.de.

== Changelog ==

= 1.5.3 =
* Fix total Consumption in SEU List.

= 1.5.2 =
* Sort SEU and Node settings lists by columns and switch between the views efficiently.

= 1.5.1 =
* Fix Logs not being written.

= 1.5.0 =
* Add SEU Node-List editor with alias and description for each node
* Add Logs to user interactions for SEU, Node-List Editor, and Regression Analysis.

= 1.4.0 =
* Add Excel download for SEU List
* Default date of SEU List is now the complete last year.
* Fixed: "Knoten" should be named "Messpunkt" in SEU List
* Display total usage on top of SEU List
* Display percentage of power consumption per node receive from API in SEU List
* Show information about available Regression Analysis in SEU List
* Add unified number formatter for displaying large numbers in a clean format. (e.g. 123234512.12 becomes 123.234.512,12)

= 1.3.1 =
* Fix link from SEU to Regression analysis page.

= 1.3.0 =
* Add [managee_seu_list] shortcode that displays an SEU list with selectable from and to date. Clicking on one of the nodes in the list will open a regression analysis view.

= 1.2.1 =
* Improve Regression Analysis with multiple Columns per Year. This also adds new Graphs for each Column. Add the name of a node to regression analysis when it is fixed using the shortcode parameter node-id.

= 1.2.0 =
* Add Regression Analysis

= 1.1.4 =
* Add parent and child information to measurement nodes

= 1.1.3 =
* Remove update information on Multisite Plugin page in Non-Network view.

= 1.1.2 =
* Update test

= 1.1.1 =
* Update test

= 1.1.0 =
* Initial release version including the API interfaces for reading and writing the API Keys to the database.

= 1.0.23 =
* Code Cleanup.

= 1.0.22 =
* Add better Script importing.

= 1.0.21 =
* Add Templates to custom page rendering!

= 1.0.20 =
* Add Custom Page rendering and auto Parsing of page files for admin and frontend pages.

= 1.0.19 =
* Code Cleanup.

= 1.0.18 =
* Change Frontend Background Color.

= 1.0.17 =
* Add Script Importer. Minor Bug Fixing.

= 1.0.16 =
* Fix Minor Bugs in php imports.

= 1.0.15 =
* Improve Internal File Structure

= 1.0.14 =
* Improve Update Process

= 1.0.13 =
* Improve Update Process

= 1.0.7 =
* Fixed a bug where updates would not be executed when installing the Updates automatically.

= 1.0.6 =
* Added a Database Updater to keep all tables up to date on future Updates.

= 1.0.5 =
* Fixed setup Problems for Plugin Update Process.

= 1.0.0 =
* Initial Release

== Upgrade Notice ==

= 1.0.5 =
* Improved the plugin so that it can automatically update from the GitLab repository.
