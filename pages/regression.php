<?php
/*
 * type: frontend
 * name: Regressionsanalyse
 * icon: dashicons-admin-plugins
 * slug: regression
 * template: theme
 */
namespace ManageEConnector;
if (!defined('WPINC')) die;

if(isset($_GET["node_id"])) {
    echo '[managee_regression node_id="'.$_GET["node_id"].'" year="'. $_GET['year'] .'"]';
} else {
    echo '[managee_regression]';
}
?>