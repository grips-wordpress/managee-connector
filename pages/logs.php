<?php
/*
 * Type:admin
 * Name:Logs
 * Icon:
 * slug:settings/logs
 */
namespace ManageEConnector;
if (!defined('WPINC')) die;
?>
<h1>Logs</h1>
<p>All debug logs of the Plugin can be found here.</p>
<hr>
<table class="wp-list-table widefat fixed striped">
    <thead>
        <tr><th style="width: 150px;">Datum</th><th>Log</th></tr>
    </thead>
    <?php
    foreach(Debug::GetLogs(1000,0) as $log) {
        echo '<tr><td style="width: 150px;">' . $log->date . '</td><td>' . $log->message . '</td></tr>';
    }
    ?>
</table>