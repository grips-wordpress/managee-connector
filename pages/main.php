<?php
/*
 * type: admin
 * name: ME-Connector
 * menu-name: Einstellungen
 * icon: dashicons-admin-plugins
 * slug: settings
 */
namespace ManageEConnector;
if (!defined('WPINC')) die;
$messstelle = Connector::GetSetting('api_messstelle');
?>
<h1>
    ManageE Connector
</h1>
<p>
    Version: <?php echo Settings::$version; ?>
</p>
<hr>
<h2>General Settings</h2>
<style>
    .id-column {
        width: 100px;
    }
</style>
<form method="post" action="<?php echo esc_html(admin_url('admin-post.php')); ?>">
    <?php Post::GetFormData("save_settings"); ?>
    <table class="wp-list-table widefat fixed striped">
        <tr>
            <td class="id-column">
                <label for="api-code">API Code</label>
            </td>
            <td>
                <input type="text" class="regular-text" name="api-code" id="api-code" value="<?php echo Connector::GetSetting('api_code'); ?>"/>
            </td>
        </tr>
        <tr>
            <td class="id-column">
                <label for="api-messstelle">ID-Messstelle</label>
            </td>
            <td>
                <input type="number" class="small-text" name="api-messstelle" id="api-messstelle" value="<?php echo $messstelle; ?>"/>
            </td>
        </tr>
    </table>
    <?php
    submit_button();
    ?>
</form>
<?php

    /*Connector::ProcessNode(Connector::getNodeTree(), function($node, $depth) {
        $prefix = "";
        for($i = 0; $i < $depth; $i++) {
            $prefix .= '-';
        }
        echo $prefix . $node["name"]."<br>";
    });*/

    //echo Connector::getNodeSelect();
?>