You can add new templates to render custom frontend pages. Simply add the template to this directory and set the
template: parameter to the name of the template file.

Example using the blank slate template:

/*
 * type: frontend
 * template: blank
 * name: Blank Page
 * slug: blank
 */