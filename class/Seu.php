<?php


namespace ManageEConnector;


use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use DateTime;

class Seu
{
    public static function init() {
        Ajax::addEndpoint("get_seu_data", function() { self::GetSeuTable(); }, false);
        Ajax::addEndpoint("get_xlsx_data", function() { self::CreateXlsx(); }, false);
        Ajax::addEndpoint("save_additional_data", function() { self::SaveAdditionalData(); }, false);
        Shortcode::register("managee_seu_list", function($attributes) {
            $dateTime = new DateTime();
            $oneYearInterval = new \DateInterval('P1Y');
            $lastYear = $dateTime->sub($oneYearInterval);
            $lastYearStart = new DateTime($lastYear->format("Y")."-01-01");
            $lastYearEnd = new DateTime($lastYear->format("Y")."-12-31");

            $start = isset($attributes['start_date'])?new DateTime($attributes['start_date']):$lastYearStart;
            $end = isset($attributes['end_date'])?new DateTime($attributes['end_date']):$lastYearEnd;
            $settings = isset($attributes['settings'])?$attributes['settings']:false;
            $settingsOnly = isset($attributes['settingsOnly'])?$attributes['settingsOnly']:false;

            return Seu::Render($start, $end, $settings, $settingsOnly);
        });
    }

    public static function CreateXlsx() {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $nodeData = Connector::getNodeList(false);

        $startDate = new DateTime($_GET["start_date"]);
        $endDate = new DateTime($_GET["end_date"]);
        $total = 0.0;
        $seuList = Connector::GetSeuList($startDate, $endDate);
        foreach ($seuList as $seuRow) {
            $total += floatval($seuRow["value"]);
        }
        $sheet->setCellValueByColumnAndRow(1, 1, 'Gesamtverbrauch:');
        $sheet->setCellValueByColumnAndRow(2, 1, $total);

        $sheet->setCellValueByColumnAndRow(1, 3, 'Messpunkt');
        $sheet->setCellValueByColumnAndRow(2, 3, 'Verbrauch');
        $sheet->setCellValueByColumnAndRow(3, 3, 'Prozent');
        $sheet->getColumnDimensionByColumn(1)->setAutoSize(true);
        $sheet->getColumnDimensionByColumn(2)->setAutoSize(true);
        $sheet->getColumnDimensionByColumn(3)->setAutoSize(true);

        $rowIndex = 4;
        foreach ($seuList as $seuRow) {
            $percent = round(floatval($seuRow["value"]) / $total * 100, 2);

            $sheet->setCellValueByColumnAndRow(1, $rowIndex, $nodeData[$seuRow["node_id"]]["name"]);
            $sheet->setCellValueByColumnAndRow(2, $rowIndex, $seuRow["value"]);
            $sheet->setCellValueByColumnAndRow(3, $rowIndex, $percent);
            $rowIndex ++;
        }

        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/ms-excel');
        header('Content-Disposition: attachment; filename="'. urlencode("table.xlsx").'"');
        $writer->save('php://output');
        Debug::log("User " . wp_get_current_user()->display_name . " Exported SEU List as .xlsx");
        wp_die();
    }

    public static function SaveAdditionalData()
    {
        global $wpdb;
        $newNodeData = $_POST["data"];

        $nodeDataTable = $wpdb->prefix . Settings::$databaseTablePrefix . "node_data";

        foreach ($newNodeData as $nodeData) {
            $nodeData["seu_visible"] = $nodeData["seu_visible"]=="1";
            $wpdb->replace($nodeDataTable, $nodeData);
        }
        echo "saved";
        Debug::log("User " . wp_get_current_user()->display_name . " Updated node Settings (SEU Visibility, Alias, and Description of nodes).");
        wp_die();
    }

    public static function Render($startDate, $endDate, $settings, $settingsOnly) {

        $allowModeSwitch = $settings;
        if($settingsOnly) $allowModeSwitch = false;
        ImportHelper::RequireScript('Chart.bundle', array());
        wp_enqueue_style('dashicons');
        ImportHelper::RequireStyle('seu', array());
        add_action( 'wp_enqueue_scripts', 'include_dashicons_font', 100 );

        wp_enqueue_script('jquery-ui-datepicker');
        //jQuery UI theme css file
        wp_enqueue_style( 'jquery-ui-datepicker-style' , '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css');



        $variables = [];
        $variables["allowModeSwitch"] = $allowModeSwitch;
        $variables["showSettings"] = $settings;
        $variables["backend"] = array('root_url' => get_home_url());
        $variables["endpoint_get_seu_data"] = Ajax::getEndpointJavascriptData("get_seu_data");
        $variables["endpoint_get_xlsx_data"] = Ajax::getEndpointJavascriptData("get_xlsx_data");
        $variables["endpoint_save_additional_data"] = Ajax::getEndpointJavascriptData("save_additional_data");
        $variables["endpoint_get_node_list_data"] = Ajax::getEndpointJavascriptData("get_node_list_data");

        ImportHelper::RequireScript('seu', $variables, ["jquery"]);

        //echo json_encode($nodeData);
        return '<div class="managee managee-seu" data-start="' . $startDate->format('Y-m-d') . '" data-end="' . $endDate->format('Y-m-d') . '"></div>';
    }

    private static function EmptyStringCompare($a, $b) {
        if(trim($a) == "") {
            if(trim($b) == "") {
                //Both empty
                return 0;
            } else {
                //A Empty
                return 1;
            }
        } else {
            if(trim($b) == "") {
                //B Empty
                return -1;
            } else {
                //Both not empty
                return strcmp(strtolower($a), strtolower($b));
            }
        }
    }

    public static function GetSeuTable() {
        global $wpdb;
        $nodeData = Connector::getNodeList(false);
        $startDate = new DateTime($_POST["startDate"]);
        $endDate = new DateTime($_POST["endDate"]);
        $allowModeSwitch = $_POST["allowModeSwitch"] == "true";
        $showSettings = $_POST["showSettings"] == "true";
        $sortBy = $_POST["sortBy"];
        $total = 0.0;

        $seuList = Connector::GetSeuList($startDate, $endDate, 1, true);
        $seuListSorted = [];

        foreach ($seuList as $node) {
            $seuListSorted[$node["node_id"]] = $node["value"];
        }

        $nodeData = Connector::getNodeList(false, true);

        $ids = array_keys($nodeData);

        for($j = 0; $j < count($ids); $j++) {
            $i = $ids[$j];
            $nodeData[$i]["consumption"] = 0.0;
            if(isset($seuListSorted[$nodeData[$i]["id"]])) {
                $nodeData[$i]["consumption"] = $seuListSorted[$nodeData[$i]["id"]];
            }

            $availableData = "";

            $query = "SELECT `column` as dataset FROM ".$wpdb->prefix . Settings::$databaseTablePrefix."regression_data WHERE `year` = ".$startDate->format("Y")." AND node_id = " . $nodeData[$i]["id"] . " GROUP BY `column`";
            $results = $wpdb->get_results( $wpdb->prepare($query));

            foreach ($results as $row) {
                if($availableData != "") {
                    $availableData .= ", ";
                }
                $availableData .= $row->dataset;
            }

            $nodeData[$i]["availableData"] = $availableData;

            $nodeData[$i]["isVisible"] = false;
            $nodeData[$i]["alias"] = "";
            $nodeData[$i]["description"] = "";

            if($nodeData[$i]["additional_data"] != null) {
                $nodeData[$i]["isVisible"] = $nodeData[$i]["additional_data"]->seu_visible == 1;
                $nodeData[$i]["alias"] = $nodeData[$i]["additional_data"]->alias;
                $nodeData[$i]["description"] = $nodeData[$i]["additional_data"]->description;
            }

            if($nodeData[$i]["isVisible"] || $showSettings) {
                $total += floatval($nodeData[$i]["consumption"]);
            }
        }

        $html = "<h4>Gesamtverbrauch: " . Utilities::formatNumber($total) . " kWh</h4>";
        $html .= '<input type="hidden" id="showSettings" value="' . ($showSettings?"true":"false") . '">';

        if($allowModeSwitch) {
            if($showSettings) {
                $html .= '<button class="button switch-mode-button" style="margin-bottom: 10px;"><span class="dashicons dashicons-list-view"></span> SEU-Liste Anzeigen</button>';
            } else {
                $html .= '<button class="button switch-mode-button" style="margin-bottom: 10px;"><span class="dashicons dashicons-admin-generic"></span> Knoteneinstellungen Anzeigen</button>';
            }
        }

        $html .= '<table class="table"><tr>';

        $nodeLink = '<a href="#" class="sort-button" data-sort="node">Messpunkt</a>';
        $analysisLink = '<a href="#" class="sort-button" data-sort="analysis">Verfügbare Analysen</a>';
        $usageLink = '<a href="#" class="sort-button" data-sort="usage">Verbrauch (kWh)</a>';
        $aliasLink = '<a href="#" class="sort-button" data-sort="alias">Alias</a>';
        $descriptionLink = '<a href="#" class="sort-button" data-sort="description">Beschreibung</a>';
        $visibilityLink = '<a href="#" class="sort-button" data-sort="visible">Sichtbar in SEU</a>';

        switch ($sortBy) {
            case "node":
                $nodeLink = 'Messpunkt <span class="dashicons dashicons-arrow-down"></span>';
                break;
            case "analysis":
                $analysisLink = 'Verfügbare Analysen <span class="dashicons dashicons-arrow-down"></span>';
                break;
            case "usage":
                $usageLink = 'Verbrauch (kWh) <span class="dashicons dashicons-arrow-down"></span>';
                break;
            case "alias":
                $aliasLink = 'Alias <span class="dashicons dashicons-arrow-down"></span>';
                break;
            case "description":
                $descriptionLink = 'Beschreibung <span class="dashicons dashicons-arrow-down"></span>';
                break;
            case "visible":
                if($showSettings) {
                    $visibilityLink = 'Sichtbar in SEU <span class="dashicons dashicons-arrow-down"></span>';
                } else {
                    $sortBy = "node";
                    $nodeLink = 'Messpunkt <span class="dashicons dashicons-arrow-down"></span>';
                }
                break;
            default:
                $sortBy = "node";
                $nodeLink = 'Messpunkt <span class="dashicons dashicons-arrow-down"></span>';
                break;
        }


        $html .= '<th style="text-align: left;">' . $nodeLink . '</th>';
        $html .= '<th style="text-align: left;">' . $analysisLink . '</th>';
        $html .= '<th style="text-align: left;">' . $usageLink . '</th>';

        if($showSettings) {
            $html .= '<th style="text-align: left;">' . $visibilityLink . '</th>';
        }

        $html .= '<th style="text-align: left;">' . $aliasLink . '</th>';
        $html .= '<th style="text-align: left;">' . $descriptionLink . '</th>';

        $html .= '</tr>';
        //$html .= 'SEU von '.$startDate->format("Y.m.d") . " bis " . $endDate->format("Y.m.d")."<br>";
        //$html .= "<div>".$total."</div>";
        /*foreach ($seuList as $seuRow) {
            $percent = round(floatval($seuRow["value"]) / $total * 100, 2);
            $availableData = "";

            $query = "SELECT `column` as dataset FROM ".$wpdb->prefix . Settings::$databaseTablePrefix."regression_data WHERE `year` = ".$startDate->format("Y")." AND node_id = ".$seuRow["node_id"] . " GROUP BY `column`";
            $results = $wpdb->get_results( $wpdb->prepare($query));

            foreach ($results as $row) {
                if($availableData != "") {
                    $availableData .= ", ";
                }
                $availableData .= $row->dataset;
            }

            $html .= '<tr class="seu-item"><td style="text-align: left;"><a href="'. get_site_url() .'/regression?node_id=' . $seuRow["node_id"] . '&year=' . $startDate->format("Y") . '"> ' . $nodeData[$seuRow["node_id"]]["name"] . '</a></td><td style="text-align: left;">' . $availableData . '</td><td style="text-align: right;">' . Utilities::formatNumber($seuRow["value"]) . ' kWh (' . Utilities::formatNumber(round($percent, 2)) . '%)</td></tr>';
        }
        $html .= '</table></div>';*/


        switch ($sortBy) {
            case "node":
                usort($nodeData, function($a, $b) {return strcmp(strtolower($a["node_name"]), strtolower($b["node_name"]));});
                break;
            case "analysis":
                usort($nodeData, function($a, $b) {
                  return self::EmptyStringCompare($a["availableData"], $b["availableData"]);
                });
                break;
            case "usage":
                usort($nodeData, function($a, $b) {return $a["consumption"] < $b["consumption"];});
                break;
            case "alias":
                usort($nodeData, function($a, $b) {
                    return self::EmptyStringCompare($a["alias"], $b["alias"]);
                });
                break;
            case "description":
                usort($nodeData, function($a, $b) {
                    return self::EmptyStringCompare($a["description"], $b["description"]);
                });
                break;
            case "visible":
                usort($nodeData, function($a, $b) {return $a["isVisible"] < $b["isVisible"];});
                break;
            default:
                usort($nodeData, function($a, $b) {return strcmp(strtolower($a["node_name"]), strtolower($b["node_name"]));});
                break;
        }


        foreach ($nodeData as $node) {
            if(!$showSettings && !$node["isVisible"]) continue;
            $checked = "";
            if($node["isVisible"]) {
                $checked = "checked";
            }

            $html .= '<tr class="node-setting" data-node-id="' . $node["id"] . '">';
            $html .= '<td><a href="'. get_site_url() .'/regression?node_id=' . $node["id"] . '&year=' . $startDate->format("Y") . '"> ' . $node["node_name"] . '</a></td>';
            $html .= '<td>' . $node["availableData"] . '</td>';
            $html .= '<td style="text-align: right;">' . Utilities::formatNumber($node["consumption"]) . '</td>';
            if($showSettings) {
                $html .= '<td style="text-align: center;"><input type="checkbox" class="node_visible_checkbox" ' . $checked . '></td>';
                $html .= '<td><input style="width: 100%;" class="alias-input" type="text" value="' . $node["alias"] . '"></td>';
                $html .= '<td><textarea class="description-input" style="width: 100%;">' . $node["description"] . '</textarea></td>';
            } else {
                $html .= '<td>' . $node["alias"] . '</td>';
                $html .= '<td>' . nl2br($node["description"]) . '</td>';
            }
            $html .= '</tr>';

        }
        $html.="</table>";
        if($showSettings) {
            $html .= "<div style='height: 100px;'>";
            $html .= '<div style="position: fixed; bottom: 0; left: 0; right: 0; padding: 15px; background-color: white; border-top: 2px solid gray;"><button style="width: 100%;" class="btn save-button"><span class="dashicons dashicons-saved"></span> Knoteneinstellungen Speichern</button></div>';
        }

        $html .= "</div>";

        echo $html;

        wp_die();
    }
}