<?php

namespace ManageEConnector;
class Utilities
{
    /**
     * Get a pointer to an internal class function. This is used to link hooks to static class functions.
     * @param $functionName
     * @return array
     */
    public static function getFunctionPointer($functionName)
    {
        return array(debug_backtrace()[1]["class"], $functionName);
    }

    /**
     * Check if a string ends with another string
     * @param string $haystack the string to search in
     * @param string $needle the ending to search for
     * @return bool true if $haystack ends with $needle
     */
    public static function stringEndsWith( $haystack, $needle ) {
        $length = strlen( $needle );
        if( !$length ) {
            return true;
        }
        return substr( $haystack, -$length ) === $needle;
    }

    public static function fillStringFront($count, $string, $char = "0") {
        while(strlen($string) < $count) {
            $string = $char . $string;
        }
        return $string;
    }

    public static function fillStringEnd($count, $string, $char = "0") {
        while(strlen($string) < $count) {
            $string = $string . $char;
        }
        return $string;
    }
    public static function formatNumber($number)
    {
        //return $number;
        $baseVal = floatVal($number);
        $prefix = "";
        if($baseVal < 0) {
            $prefix = "- ";
            $baseVal = -$baseVal;
        }

        $baseVal = "" . $baseVal;
        $split = explode(".", $baseVal);
        $full = $split[0];
        $point = $split[1];

        $fullChars = $full . "";
        $fullString = "";
        $pCount = 0;
        for($i = strlen($fullChars)-1; $i >= 0; $i --) {
            $pCount ++;
            $fullString = $fullChars[$i] .  $fullString;
            if($pCount == 3 && $i > 0) {
                $fullString =  "." . $fullString;
                $pCount = 0;
            }
        }

        return  $prefix . $fullString . "," .  substr(self::fillStringEnd(2, $point), 0, 2);
    }
}