<?php


namespace ManageEConnector;

use DateTime;

class Connector
{
    public static function GetSetting($name) {
        global $wpdb;
        $settingsTable = $wpdb->prefix . Settings::$databaseTablePrefix . "settings";
        $queryResult = $wpdb->get_var("SELECT `value` FROM " . $settingsTable . " WHERE id = '". $name . "';");
        return $queryResult;
    }

    public static function SetSetting($id, $value) {
        global $wpdb;

        $wpdb->update(
            $wpdb->prefix . Settings::$databaseTablePrefix . "settings",
            array(
                'value' => $value,
            ),
            array(
                'id' => $id,
            )
        );
    }

    public static function processNode($node, $function, $depth = 0, $parentId = -1) {
        if($node == null) return;
        $function($node, $depth, $parentId);
        if($node["children"] != null) {
            foreach ($node["children"] as $key=>$child) {
                self::processNode($child, $function, $depth + 1, $node["NodeID"]);
            }
        }
    }

    public static function getNodeTree()
    {
        return json_decode(Connector::GetSetting("current_nodes"), true);
    }

    public static function getNodeList($forSelect, $includeSeuData = true)
    {
        $rootNode = self::getNodeTree();
        $nodes = array();
        $additionalNodeData = array();
        if($includeSeuData) {
            $additionalNodeData = self::GetAdditionalNodeData();
        }
        if($rootNode != null) {
            self::processNode($rootNode, function($node, $depth, $parentId) use (&$nodes, $forSelect, $additionalNodeData) {
                $childrenIds = [];
                if(isset($node["children"])) {
                    foreach ($node["children"] as $child) {
                        $childrenIds[] = $child["NodeID"];
                    }
                }
                $name = $node["name"];
                if(isset($additionalNodeData[$node["NodeID"]])) {
                    if($additionalNodeData[$node["NodeID"]]->alias != "") {
                        $name = $additionalNodeData[$node["NodeID"]]->alias;
                    }
                }
                $nodeData = array(
                    "name"=>$name,
                    "depth"=>$depth,
                    "tooltip"=>$node["Tooltip"],
                    "id"=>$node["NodeID"],
                    "children"=>$childrenIds,
                    "parentId"=>$parentId,
                    "channel"=>explode(",",$node["channel"]),
                    "node_name"=>$node["name"],
                    "additional_data"=>$additionalNodeData[$node["NodeID"]]
                );
                if($forSelect) {
                    $nodes[] = $nodeData;
                } else {
                    $nodes[$node["NodeID"]] = $nodeData;
                }
            });
        }
        return $nodes;
    }

    public static function getNodeSelect($id = "node-select", $name = "node-select", $hierarchy = true) {
        $select = '<select id="'.$id.'">';
        $select .= '<option value="">Kein Knoten gewählt.</option>';

        Connector::ProcessNode(Connector::getNodeTree(), function($node, $depth, $parentId) use (&$select) {
            $prefix = "";
            for($i = 0; $i < $depth; $i++) {
                $prefix .= '&nbsp;';
            }
            $select .= '<option value="'.$node["NodeID"].'">' . $prefix.$node["name"] . '</option>';
        });

        $select .= '</select>';
        return $select;
    }

    public static function createLocationSelect($id = "locationSelect") {
        $code = self::GetSetting("api_code");
        $messtelle = self::GetSetting("api_messstelle");

        $locations = json_decode(self::apiRequest($code, "Messstellen/".$messtelle)['content'], true);

        echo '<select id="'.$id.'">';

        foreach ($locations as $location) {
            echo '<option value="'.$location["Messstelle"].'">' . $location["Bezeichnung"] . '</option>';
        }
        echo '</select>';
    }

    /**
     * @param DateTime $startDate seu start date
     * @param DateTime $endDate seu end date
     * @param int $type
     * @return mixed
     */
    public static function GetSeuList($startDate, $endDate, $type = 1, $all = false) {
        $code = self::GetSetting("api_code");
        $messtelle = self::GetSetting("api_messstelle");
        $requestUrl = "SEU/" . $messtelle . "/" . $type . "/" . $startDate->format("Ymd") . "/" . $endDate->format("Ymd");

        $allNodes = self::getNodeList(false, true);

        $seuNodes = json_decode(self::apiRequest($code, $requestUrl)["content"], true);


        $seuNodesSorted = array();

        foreach ($seuNodes as $seuNode) {
            $seuNodesSorted[$seuNode["node_id"]] = $seuNode["value"];
        }

        $seuList = array();

        foreach ($allNodes as $node) {
            if(!$all && ($node["additional_data"] == null || $node["additional_data"]->seu_visible != "1")) continue;
            $seuList[] = array("node_id" => $node["id"], "value"=>(isset($seuNodesSorted[$node["id"]])?$seuNodesSorted[$node["id"]]:0.00));
        }

        return $seuList;
    }

    public static function GetAdditionalNodeData() {
        global $wpdb;
        $nodeDataTable = $wpdb->prefix . Settings::$databaseTablePrefix . "node_data";
        $seuDataQuery = $wpdb->get_results("SELECT * FROM ".$nodeDataTable);
        $additionalNodeData = array();
        foreach ($seuDataQuery as $nodeData) {
            $additionalNodeData[$nodeData->node_id] = $nodeData;
        }
        return $additionalNodeData;
    }

    public static function request($request) {
        $code = self::GetSetting("api_code");
        $messtelle = self::GetSetting("api_messstelle");

        return self::apiRequest($code, $request)['content'];
    }

    private static function apiRequest($code, $request) {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "",       // handle all encodings
            CURLOPT_USERAGENT      => "spider", // who am i
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
            CURLOPT_TIMEOUT        => 120,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => false     // Disabled SSL Cert checks
        );

        $url = "https://restapi20190624033927.azurewebsites.net/api/" . $request . "?code=" . $code;
        //echo $url;
        $ch = curl_init($url);
        curl_setopt_array( $ch, $options );
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['content'] = $content;
        return $header;
    }
}