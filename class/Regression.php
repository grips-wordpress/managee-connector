<?php


namespace ManageEConnector;


use DateTime;

class Regression
{
    private static $pageChartId = 0;
    private static $chartData = [];

    public static function init() {
        Ajax::addEndpoint("get_node_regression_data", function() { self::getNodeRegressionData(); }, false);
        Ajax::addEndpoint("set_node_regression_data", function() { self::setNodeRegressionData(); }, false);

        Shortcode::register("managee_regression", function($attributes) {
            $nodeId = isset($attributes['node_id'])?$attributes['node_id']:null;
            $year = isset($attributes['year'])?$attributes['year']:((new Datetime())->format("Y"));
            return Regression::Render($nodeId, $year);
        });
    }

    public static function getNodeRegressionData() {
        global $wpdb;
        $year = intval($_POST["year"]);
        $nodeId = intval($_POST["node_id"]);
        $data = [];
        for($i = 0; $i < 12; $i++) {
            $data[] =
            [
                "energy"=>0
            ];
        }
        $rawData = json_decode(Connector::request("Tageswerte/" . Connector::GetSetting("api_messstelle") . "/" . $nodeId . "/".$year."0101/".$year."1231"), true);
        foreach ($rawData as $dayData) {
            $data[intval(explode("-",$dayData["date"])[1])-1]["energy"] += $dayData["value"];
        }

        $results = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM ".$wpdb->prefix . Settings::$databaseTablePrefix."regression_data WHERE `year` = ".$year." AND node_id = ".$nodeId));



        $columns = [];

        foreach ($results as $row) {
            if(!in_array($row->column, $columns)) {
                $columns[] = $row->column;
            }
            $data[$row->month]["columns"][$row->column] = $row->value;
        }

        for($i = 0; $i < 12; $i++) {
            $data[$i]["energy"] = round($data[$i]["energy"] ,2);
        }
        header('Content-Type: application/json');
        echo json_encode(array("data"=>$data, "columns"=>$columns, "raw"=>json_encode($results)));
        //End execution for custom output
        wp_die();
    }

    public static function setNodeRegressionData() {
        global $wpdb;
        $year = intval($_POST["year"]);
        $nodeId = intval($_POST["node_id"]);
        $columnIds = [];
        if($year > 0 && $nodeId >= 0) {
            if(is_array($_POST["data"])) {
                $wpdb->delete(
                    $wpdb->prefix . Settings::$databaseTablePrefix."regression_data",
                    array(
                        'node_id' => $nodeId,
                        'year' => $year
                    )
                );
                $i = 0;
                foreach ($_POST["data"]["columns"] as $column) {
                    $columnIds[$i] = $column;
                    $i ++;
                }

                for($i = 0; $i < 12; $i++) {
                    $month = $i;
                    for($j = 0; $j < count($columnIds); $j++) {
                        $value = $_POST["data"]["months"][$i][$j];
                        $column = $columnIds[$j];
                        echo $year."-".$month."-'".$column."': ".$value."<br>";
                        $wpdb->replace(
                            $wpdb->prefix . Settings::$databaseTablePrefix."regression_data",
                            array(
                                'node_id' => $nodeId,
                                'year' => $year,
                                'month' => $month,
                                'column' => $column,
                                'value' => $value
                            ),
                            array(
                                '%d',
                                '%d',
                                '%d',
                                '%s',
                                '%s'
                            )
                        );
                    }
                }
                $nodeList = Connector::getNodeList(false, true);

                Debug::log("User " . wp_get_current_user()->display_name . " updated regression-data of Node '" . $nodeList[$nodeId]["node_name"] . "' for Year " . $year);

            }
        }

        //End execution for custom output
        wp_die();
    }

    public static function Render($nodeId, $year) {

        self::$pageChartId ++;
        self::$chartData[] = array(
            "chartId"=>self::$pageChartId,
            "nodeId"=>$nodeId,
            "year"=>$year
        );

        ImportHelper::RequireScript('Chart.bundle', array());
        wp_enqueue_style('dashicons');
        add_action( 'wp_enqueue_scripts', 'include_dashicons_font', 100 );
        $variables = [];

        $variables["backend"] = array('root_url' => get_home_url());
        $variables["chart_data"] = self::$chartData;
        $variables["current_year"] = [date("Y")];
        $variables["endpoint_get_node_regression_data"] = Ajax::getEndpointJavascriptData("get_node_regression_data");
        $variables["endpoint_put_node_regression_data"] = Ajax::getEndpointJavascriptData("set_node_regression_data");
        $variables["measurement_nodes"] = Connector::getNodeList(true);

        ImportHelper::RequireScript('regression', $variables, ["jquery"]);

        $html = '<h3>Regressionsanalyse</h3>';
        $html .= '<div class="regression" style="position: relative; overflow: hidden;" id="regression_' . self::$pageChartId . '">';

        if($nodeId == null) {
            //Free select mode
        } else {
            // Fixed mode
            $nodeData = Connector::getNodeList(false)[$nodeId];
            $nodeAlias = $nodeData["name"];
            $nodeName = $nodeData["node_name"];
            $html .= '<h5>Knoten: '.$nodeAlias."</h5>";
            if($nodeAlias != $nodeName) {
                $html .= '<h6>Messpunkt: '.$nodeName."</h6>";
            }
            if(isset($nodeData["additional_data"]) && trim($nodeData["additional_data"]->description) != "") {

                $description = $nodeData["additional_data"]->description;
                $html .= '<p><i>'.nl2br($description)."</i></p><br>";
            }
        }

        $html .= '<style>.</style>';

        $html .= '<div style="" class="main-container">';
        $html .= '    <div style="min-width: 49%; flex: 1;">';
        $html .= '    <div class="select-div"></div>';
        $html .= '        <table style="width: auto;" class="regression-data-table">';
        $html .= '       </table>';
        $html .= '   </div>';
        $html .= '   <button class="save-button"><span class="dashicons dashicons-cloud-saved"></span> Änderungen Speichern</button>';
        $html .= '   <div class="chart-container"></div>';
        $html .= '</div>';


        $html .= '<div style="display: none; text-align: center; position: absolute; left: 0; top: 0; right: 0; bottom: 0; padding-top: 200px; font-size: 30px; background-color: #cfcfcf;" class="loading-container">Daten werden geladen...</div>';

        $html .= '</div>';

        return $html;
    }
}