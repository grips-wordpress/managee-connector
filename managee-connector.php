<?php
/*
Plugin Name: ManageE Connector
Plugin URI: https://managee.de
Description: ManageE Connector
Author: Florian Bayer - GriPS Automation GmbH
Version: 1.5.3
Author URI: https://grips-automation.com
*/
namespace ManageEConnector;

if ( !class_exists( 'Managee_Connector_Plugin' ) ) {

    require_once 'autoload.php';
    require_once 'vendor/autoload.php';

    class Managee_Connector_Plugin
    {
        /**
         * Entry point of the Plugin called at the Bottom of this script.
         */
        public static function init() {
            // Initialize the Plugin
            Plugin::init(__FILE__, 'https://gitlab.com/grips-wordpress/managee-connector/');
            Post::RegisterPostAction('save_settings', Utilities::getFunctionPointer('saveSettings'));

            Regression::init();
            Seu::init();
        }

        public static function saveSettings() {
            Connector::SetSetting("api_code", $_POST['api-code']);
            Connector::SetSetting("api_messstelle", $_POST['api-messstelle']);

            $nodes = Connector::request("/Treedata/".$_POST['api-messstelle']);
            Connector::SetSetting("current_nodes", $nodes);

            header('Location: '.admin_url('admin.php').'?page=managee-connector-settings');
        }
    }

    Managee_Connector_Plugin::init();
}
